package org.javasavvy.tutorial.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.javasavvy.tutorial.entity.Airways;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("airwaysDao")
@Transactional(propagation=Propagation.REQUIRED)
public class AirwaysDaoImpl implements AirwaysDao{
	
	@PersistenceContext
	public EntityManager entityManager;

	@Override
	public List<Airways> getFlightDetails(String flightNo) {
		List<Airways> resultList = entityManager.createNamedQuery("getFlights",Airways.class).
				setParameter("fightNo", flightNo).getResultList();
		
		return resultList;
	}
	@Override
	public List<Airways> getAllFlightDetails() {
		List<Airways> resultList = entityManager.createNamedQuery("getAllFlights",Airways.class)
				.getResultList();
		return resultList;
	}
	@Override
	public List<Airways> getByAirlines(String airlines) {
		List<Airways> resultList = entityManager.createNamedQuery("getByAirlines",Airways.class)
				.setParameter("airlines", airlines).getResultList();
		return resultList;
	}
	
	
	

}
