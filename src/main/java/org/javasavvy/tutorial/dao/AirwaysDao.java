package org.javasavvy.tutorial.dao;

import java.util.List;

import org.javasavvy.tutorial.entity.Airways;
import org.springframework.stereotype.Repository;

@Repository
public interface AirwaysDao {
	
	public List<Airways> getFlightDetails(String flightNo);
	public List<Airways> getAllFlightDetails();
	public List<Airways> getByAirlines(String airlines);

}
