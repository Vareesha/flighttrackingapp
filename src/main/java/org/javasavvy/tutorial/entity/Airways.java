package org.javasavvy.tutorial.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "AIRWAYS", schema = "HCP_RAJ")
@NamedQueries({
	@NamedQuery(name = "getFlights", query = "select aw from Airways aw where aw.flight_no=:fightNo"),
	@NamedQuery(name = "getAllFlights", query = "select aw from Airways aw"),
	@NamedQuery(name = "getByAirlines", query = "select aw from Airways aw where aw.airlines=:airlines"),
	
})
public class Airways implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="FLIGHT_NO")
	private String flight_no;
	
	@Column(name="AIRLINES")
	private String airlines;
	
	@Column(name="FLIGHT_NAME")
	private String flight_name;
	
	@Column(name="DEPARTURE")
	private String departure;
	
	@Column(name="ARRIVAL")
	private String arrival;
	
	@Column(name="ARRIVAL_GATE")
	private String arrival_gate;
	
	@Column(name="STATUS")
	private String status;

	public String getFlight_no() {
		return flight_no;
	}

	public void setFlight_no(String flight_no) {
		this.flight_no = flight_no;
	}

	public String getAirlines() {
		return airlines;
	}

	public void setAirlines(String airlines) {
		this.airlines = airlines;
	}

	public String getFlight_name() {
		return flight_name;
	}

	public void setFlight_name(String flight_name) {
		this.flight_name = flight_name;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public String getArrival() {
		return arrival;
	}

	public void setArrival(String arrival) {
		this.arrival = arrival;
	}

	public String getArrival_gate() {
		return arrival_gate;
	}

	public void setArrival_gate(String arrival_gate) {
		this.arrival_gate = arrival_gate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
}
