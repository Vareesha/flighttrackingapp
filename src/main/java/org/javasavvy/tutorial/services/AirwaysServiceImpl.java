package org.javasavvy.tutorial.services;

import java.util.List;

import org.javasavvy.tutorial.dao.AirwaysDao;
import org.javasavvy.tutorial.dao.AirwaysDaoImpl;
import org.javasavvy.tutorial.entity.Airways;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("airwaysService")
@Transactional(propagation=Propagation.REQUIRED)
public class AirwaysServiceImpl implements AirwaysService{
	
	@Autowired
	AirwaysDao airwaysDao;
	
	public List<Airways> getFlightDetails(String flightNo ) {
		return airwaysDao.getFlightDetails(flightNo);
	}

	@Override
	public List<Airways> getAllFlightDetails() {
		return airwaysDao.getAllFlightDetails();
	}

	@Override
	public List<Airways> getByAirlines(String airlines) {
		return airwaysDao.getByAirlines(airlines);
	}

}
