package org.javasavvy.tutorial.services;

import java.util.List;

import org.javasavvy.tutorial.entity.Airways;

public interface AirwaysService {
	public List<Airways> getFlightDetails(String flightNo);
	public List<Airways> getAllFlightDetails();
	public List<Airways> getByAirlines(String airlines);

}
