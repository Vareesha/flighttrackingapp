package org.javasavvy.rest.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.javasavvy.rest.modal.UserModal;
import org.javasavvy.tutorial.dao.UserDAO;
import org.javasavvy.tutorial.entity.Airways;
import org.javasavvy.tutorial.entity.User;
import org.javasavvy.tutorial.services.AirwaysService;
import org.javasavvy.tutorial.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


@Component
@Path("/user")
public class UserRestResource {

	
	@Autowired
	@Qualifier("userService")
	private UserService userService;
	
	@Autowired
	@Qualifier("airwaysService")
	private AirwaysService airwaysService;
	
	@Autowired(required=false)
	private UserDAO userDao;
	
	
	Gson gson = new GsonBuilder().setPrettyPrinting().create();
	
	@GET
	@Path("/user-json-meta")
	public UserModal getUser(){		
		UserModal userModal = new UserModal();
		userModal.setEmail("jajfaddf");
		userModal.setFirstName("jayaram");
		userModal.setLastName("poks");
		userModal.setPassword("passwd");
		userModal.setSex("male");
		userModal.setUserId(101);
		return userModal;
	}
	
	@GET
    @Path("/download/doc")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)

    public Response downloadDocFile() {
 
        // set file (and path) to be download
        File file = new File("C:/Users/vareesha.subbanna/Documents/RESUME_Sample.docx");
 
        ResponseBuilder responseBuilder = Response.ok((Object) file);
        responseBuilder.header("Content-Disposition", "attachment; filename=\"MyJerseyDocFile.docx\"");
        return responseBuilder.build();
    }
 
	@GET
    @Path("/saveUser")
	@Produces(MediaType.APPLICATION_JSON)
    public Response deleteUerById() {
		User user =new User();
		user.setPwd("TEST2");
		user.setStatus("TEST2");
		user.setUser_name("TEST2");
		
		User userRs = userDao.save(user);
		return Response.status(201).entity(gson.toJson(userRs)).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.header("Access-Control-Allow-Headers",
						"X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept,Referer")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true").build();
	}
	@GET
    @Path("/updateUser")
	@Produces(MediaType.APPLICATION_JSON)
    public Response updateUser() {
		User user =new User();
		//user.setPwd("TEST2");
		user.setStatus("TEST3");
		user.setUser_name("TEST");
		
		 userDao.merge(user);
		return Response.status(201).entity(gson.toJson("")).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.header("Access-Control-Allow-Headers",
						"X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept,Referer")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true").build();
	}
	
	@GET
	@Path("/getFlightDetails")
	@Produces(MediaType.APPLICATION_JSON)
	 public Response getFlightDetails(@QueryParam("flightNo") String flightNo ) {
		List<Airways> flightDetails = airwaysService.getFlightDetails(flightNo);
		return Response.status(201).entity(gson.toJson(flightDetails)).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.header("Access-Control-Allow-Headers",
						"X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept,Referer")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true").build();
		
	}
	
	@GET
	@Path("/getAllFlightDetails")
	@Produces(MediaType.APPLICATION_JSON)
	 public Response getAllFlightDetails() {
		List<Airways> flightDetails = airwaysService.getAllFlightDetails();
		return Response.status(201).entity(gson.toJson(flightDetails)).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.header("Access-Control-Allow-Headers",
						"X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept,Referer")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true").build();
		
	}
	
	@GET
	@Path("/getLiveDetails")
	@Produces(MediaType.APPLICATION_JSON)
	 public Response getLiveDetails() throws ClientProtocolException, IOException {

		HttpClient client = new DefaultHttpClient();
		
		  HttpGet request = new HttpGet("https://public-api.adsbexchange.com/VirtualRadar/AircraftList.json?fCouQ=India");
		  HttpResponse response = client.execute(request);
		  
          HttpEntity responseEntity = response.getEntity();
          String responseBody = EntityUtils.toString(responseEntity);
          JsonParser parser = new JsonParser();
          JsonObject jObj = (JsonObject) parser.parse(responseBody);
          JsonArray experience = (JsonArray) jObj.get("acList");
          JsonArray finalJarray=new JsonArray();
          for(int i=0;i<experience.size();i++){
          JsonObject jObj1=(JsonObject)experience.get(i);
          if(null !=jObj1.get("From")){
        	  JsonObject jOb=new JsonObject();
          jOb.add("Id", jObj1.get("Id"));
          jOb.add("Lat", jObj1.get("Lat"));
          jOb.add("Long", jObj1.get("Long"));
          jOb.add("PosTime", jObj1.get("PosTime"));
          jOb.add("FSeen", jObj1.get("FSeen"));
          jOb.add("From", jObj1.get("From"));
          jOb.add("To", jObj1.get("To"));
          jOb.add("Op", jObj1.get("Op"));
          finalJarray.add(jOb);
          }
          }
return Response.status(201).entity(gson.toJson(finalJarray)).header("Access-Control-Allow-Origin", "*")
		.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
		.header("Access-Control-Allow-Headers",
				"X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept,Referer")
		.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
		.header("Access-Control-Allow-Credentials", "true").build();

	}
	
	@GET
	@Path("/getByAirlines")
	@Produces(MediaType.APPLICATION_JSON)
	 public Response getByAirlines(@QueryParam("airlines") String airlines ) {
		List<Airways> flightDetails = airwaysService.getByAirlines(airlines);
		return Response.status(201).entity(gson.toJson(flightDetails)).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.header("Access-Control-Allow-Headers",
						"X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept,Referer")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true").build();
		
	}
}
	
